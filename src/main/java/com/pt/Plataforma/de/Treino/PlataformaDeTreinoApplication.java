package com.pt.Plataforma.de.Treino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlataformaDeTreinoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlataformaDeTreinoApplication.class, args);
	}

}
